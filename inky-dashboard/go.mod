module handler/function

go 1.19

require (
	github.com/fogleman/gg v1.3.0
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	golang.org/x/exp v0.0.0-20230306221820-f0f767cdffd6
	golang.org/x/image v0.6.0
)
