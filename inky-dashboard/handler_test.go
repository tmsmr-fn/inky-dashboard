package function

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func TestCallHandle(t *testing.T) {
	data, err := ReqTestData()
	if err != nil {
		t.Fatal(err)
	}
	body, err := json.Marshal(data)
	if err != nil {
		t.Fatal(err)
	}
	reader := bytes.NewReader(body)
	req, err := http.NewRequest("POST", "/", reader)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Add("Fn-Api-Key", "insecure")
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(Handle)
	handler.ServeHTTP(rr, req)
	if rr.Code != http.StatusOK {
		t.Fatal("unexpected status code")
	}
	out, err := os.Create("/tmp/out.jpg")
	if err != nil {
		t.Fatal(err)
	}
	defer out.Close()
	_, err = io.Copy(out, rr.Body)
	if err != nil {
		t.Fatal(err)
	}
}
