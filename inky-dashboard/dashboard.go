package function

import (
	"errors"
	"fmt"
	"github.com/fogleman/gg"
	"github.com/golang/freetype/truetype"
	"golang.org/x/image/font"
	"golang.org/x/image/font/gofont/gomedium"
	_ "golang.org/x/image/font/gofont/gomedium"
	_ "golang.org/x/image/font/gofont/gomono"
	_ "golang.org/x/image/font/gofont/goregular"
	"image"
	"image/jpeg"
	"io"
	"math"
	"net/http"
	"strconv"
	"time"
)

const (
	tzName = "Europe/Berlin"
)

var (
	weekdays = []string{"Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"}
	months   = []string{"", "Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"}
)

type Canvas struct {
	ctx  *gg.Context
	fn24 font.Face
	fn28 font.Face
	fn36 font.Face
	fn48 font.Face
}

func (c *Canvas) clear() {
	c.ctx.SetRGB(1, 1, 1)
	c.ctx.Clear()
}

func (c *Canvas) drawStrGray(x float64, y float64, text string, font font.Face, rgb float64) {
	c.ctx.SetRGB(rgb, rgb, rgb)
	c.ctx.SetFontFace(font)
	c.ctx.DrawStringAnchored(text, x, y, 0, 1)
}

func (c *Canvas) drawRectGray(x float64, y float64, w float64, h float64, rgb float64) {
	c.ctx.SetRGB(rgb, rgb, rgb)
	c.ctx.DrawRectangle(x, y, w, h)
	c.ctx.Fill()
}

func NewCanvas() (*Canvas, error) {
	f, err := truetype.Parse(gomedium.TTF)
	if err != nil {
		return nil, err
	}
	return &Canvas{
		ctx:  gg.NewContext(800, 480),
		fn24: truetype.NewFace(f, &truetype.Options{Size: 24}),
		fn28: truetype.NewFace(f, &truetype.Options{Size: 28}),
		fn36: truetype.NewFace(f, &truetype.Options{Size: 36}),
		fn48: truetype.NewFace(f, &truetype.Options{Size: 48}),
	}, nil
}

func _timeStr(time time.Time) string {
	return fmt.Sprintf("%02d:%02d", time.Hour(), time.Minute())
}

func _roundNumStr(val float64) string {
	return strconv.Itoa(int(math.Round(val)))
}

func _downloadPng(url string) (image.Image, error) {
	res, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	if res.StatusCode != http.StatusOK {
		return nil, errors.New("unexpected status while image download")
	}
	defer res.Body.Close()
	im, format, err := image.Decode(res.Body)
	if format != "png" {
		return nil, errors.New("image format is not PNG")
	}
	return im, nil
}

type Dashboard struct {
	Quality int
	Date    string
	Weather OwmData
}

func (d Dashboard) render(w io.Writer) error {
	canvas, err := NewCanvas()
	if err != nil {
		return err
	}
	canvas.clear()
	// date
	canvas.drawStrGray(10, 10, d.Date, canvas.fn36, 0.25)
	// max / min temp
	canvas.drawStrGray(10, 50,
		fmt.Sprintf("%s / %s°C", _roundNumStr(d.Weather.TempMax), _roundNumStr(d.Weather.TempMin)),
		canvas.fn48, 0)
	// 6h detail (skip 1 3h step)
	y := 120.0
	dy := 90.0
	bg := 0.8
	for i, wd := range d.Weather.Detail {
		if i%2 == 1 {
			continue
		}
		if bg == 0.8 {
			bg = 0.7
		} else {
			bg = 0.8
		}
		// background
		canvas.drawRectGray(10, y, 310, dy, bg)
		// time
		canvas.drawStrGray(20, y+8, _timeStr(wd.Dt), canvas.fn28, 0)
		// icon
		icon, err := _downloadPng(wd.Icon)
		if err != nil {
			return err
		}
		canvas.ctx.DrawImage(icon, 90, int(y)-6)
		// temperature
		canvas.drawStrGray(200, y+8, fmt.Sprintf("%s°C", _roundNumStr(wd.Temp)), canvas.fn28, 0)
		// wind
		canvas.drawStrGray(20, y+48, fmt.Sprintf("%sm/s", _roundNumStr(wd.Wind)), canvas.fn24, 0.25)
		// precipitation
		canvas.drawStrGray(200, y+48, fmt.Sprintf("%smm/h", fmt.Sprintf("%.1f", wd.Qpf)), canvas.fn24, 0.25)
		y += dy
	}
	err = jpeg.Encode(w, canvas.ctx.Image(), &jpeg.Options{Quality: d.Quality})
	if err != nil {
		return err
	}
	return nil
}

func NewDashboard(data *ReqData) (*Dashboard, error) {
	if len(data.Owm.Detail) != 6 {
		return nil, errors.New("unexpected count of detail entries for weather")
	}
	if data.Quality < 0 || data.Quality > 100 {
		return nil, errors.New("unexpected value for quality")
	}
	tz, err := time.LoadLocation(tzName)
	if err != nil {
		return nil, err
	}
	now := time.Now().In(tz)
	return &Dashboard{
		Quality: data.Quality,
		Date:    fmt.Sprintf("%s, %d %s", weekdays[now.Weekday()], now.Day(), months[now.Month()]),
		Weather: data.Owm,
	}, nil
}
