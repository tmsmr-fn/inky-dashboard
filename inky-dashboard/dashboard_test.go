package function

import (
	"encoding/json"
	"os"
	"testing"
)

func OwmTestData() string {
	return `{
  "from": "2023-03-05T21:00:00Z",
  "to": "2023-03-06T12:00:00Z",
  "tempMin": -0.23,
  "tempMax": 4.43,
  "detail": [
    {
      "dt": "2023-03-05T21:00:00Z",
      "temp": 1.72,
      "hum": 75,
      "qpf": 0,
      "wind": 1.42,
      "icon": "https://openweathermap.org/img/wn/04n@2x.png"
    },
    {
      "dt": "2023-03-06T00:00:00Z",
      "temp": 0.69,
      "hum": 76,
      "qpf": 0,
      "wind": 1.59,
      "icon": "https://openweathermap.org/img/wn/04n@2x.png"
    },
    {
      "dt": "2023-03-06T03:00:00Z",
      "temp": -0.23,
      "hum": 78,
      "qpf": 0,
      "wind": 1.63,
      "icon": "https://openweathermap.org/img/wn/04n@2x.png"
    },
    {
      "dt": "2023-03-06T06:00:00Z",
      "temp": -0.19,
      "hum": 77,
      "qpf": 0,
      "wind": 1.56,
      "icon": "https://openweathermap.org/img/wn/04d@2x.png"
    },
    {
      "dt": "2023-03-06T09:00:00Z",
      "temp": 2.8,
      "hum": 62,
      "qpf": 0,
      "wind": 3.31,
      "icon": "https://openweathermap.org/img/wn/04d@2x.png"
    },
    {
      "dt": "2023-03-06T12:00:00Z",
      "temp": 4.43,
      "hum": 54,
      "qpf": 0,
      "wind": 4.16,
      "icon": "https://openweathermap.org/img/wn/04d@2x.png"
    }
  ]
}`
}

func ReqTestData() (*ReqData, error) {
	owm := OwmData{}
	err := json.Unmarshal([]byte(OwmTestData()), &owm)
	if err != nil {
		return nil, err
	}
	return &ReqData{
		Quality: 80,
		Owm:     owm,
	}, nil
}

func TestRenderDashboard(t *testing.T) {
	data, err := ReqTestData()
	if err != nil {
		t.Fatal(err)
	}
	dashboard, err := NewDashboard(data)
	if err != nil {
		t.Fatal(err)
	}
	out, err := os.Create("/tmp/out.jpg")
	defer out.Close()
	if err != nil {
		t.Fatal(err)
	}
	if dashboard.render(out) != nil {
		t.Fatal(err)
	}
}
