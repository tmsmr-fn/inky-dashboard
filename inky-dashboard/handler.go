package function

import (
	"encoding/json"
	"golang.org/x/exp/slices"
	"log"
	"net/http"
	"os"
	"strings"
)

var (
	fnKeys []string
)

func init() {
	val, present := os.LookupEnv("fn_api_keys")
	if present {
		fnKeys = strings.Split(val, ",")
	}
}
func Handle(w http.ResponseWriter, r *http.Request) {
	if fnKeys != nil {
		valid := slices.Contains(fnKeys, r.Header.Get("Fn-Api-Key"))
		if !valid {
			http.Error(w, "", http.StatusUnauthorized)
			return
		}
	}
	var data ReqData
	err := json.NewDecoder(r.Body).Decode(&data)
	defer r.Body.Close()
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	dashboard, err := NewDashboard(&data)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if dashboard.render(w) != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
