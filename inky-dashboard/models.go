package function

import "time"

type OwmDataDetail struct {
	Dt   time.Time `json:"dt"`
	Temp float64   `json:"temp"`
	Hum  int       `json:"hum"`
	Qpf  float64   `json:"qpf"`
	Wind float64   `json:"wind"`
	Icon string    `json:"icon"`
}

type OwmData struct {
	From    time.Time       `json:"from"`
	To      time.Time       `json:"to"`
	TempMin float64         `json:"tempMin"`
	TempMax float64         `json:"tempMax"`
	Detail  []OwmDataDetail `json:"detail"`
}

type ReqData struct {
	Quality int     `json:"quality"`
	Owm     OwmData `json:"owm"`
}
